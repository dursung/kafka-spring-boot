# kafka-spring

docker-compose up -d

docker exec -it 0234d57b9d5e bash

kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic users2

# sending message without key
kafka-console-producer --broker-list localhost:9092 --topic users2

# sending message with key
kafka-console-producer --broker-list localhost:9092  users2 --property "key.separator=-" --property "parse.key=true"

# sending message without key
kafka-console-consumer -bootstrap-server 127.0.0.1:9092 -topic users2 -from-beginning

# sending message with key
kafka-console-consumer -bootstrap-server 127.0.0.1:9092 -topic users2 -from-beginning --property "key.separator=-" --property "print.key=true"

- Consumer have three options to read
    -- from-beginning
    -- latest
    -- specific offset


# Consumer group

kafka-consumer-groups  --bootstrap-server=localhost:9092 --list

kafka-console-consumer --bootstrap-server=localhost:9092 --topic users --from-beginning

